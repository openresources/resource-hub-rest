# Resource Hub REST

This Drupal module provides a number of Drupal REST plugins which provide a convenient interface to create ResourceHub resources and the associated Drupal entities. The API is documented, and validated, using an OpenAPI V3 schema which can be browsed via [SwaggerUI](./swagger.yml).

## Installation

This module depends on the [Key Authentication Drupal module](https://www.drupal.org/project/key_auth) and the [OpenAPI PSR-7 Message (HTTP Request/Response) Validator](https://github.com/thephpleague/openapi-psr7-validator), both of which will be installed if you install this module using Composer:

`composer require openresources/resourcehub_rest`

Following installation there will be a number of Drupal configuration changes which you will probably want to export to version control.

## Use

### Authentication 

Before you can make requests to the REST API you'll need to obtain an API key. API keys are associated with Drupal users and it's the user's Drupal permissions which will determine write access via the API so you'll need to make sure the user has one of the `Site administrator` or `Content editor` roles. In order to obtain the API key, navigate to the user page, e.g. `user/1234/edit`, and click on the `Key authentication` tab. From here you'll need to `Generate a new key`.

### Making requests

There are some CURL examples below but please see [SwaggerUI](./swagger.yml) for detailed documentation on the available endpoints and the request parameters.

#### /resource

```
curl --request POST \
  --url 'http://resourcehub.lndo.site/resource?_format=json' \
  --header 'Content-Type: application/json' \
  --header 'x-api-key: a593d7966687162200bec7929aac03a9' \
  --data '
{
  "published": false,
  "title": "Testing title",
  "summary": "<p>Testing <strong>summary</strong> out</p>",
  "imageId": "c051c2f6-6aca-46f8-85cc-97e191537cac",
  "audienceIntro": "string",
  "primaryResource": {
    "sectionType": "document",
    "title": "A title",
    "documentId": "b38da175-3c84-4bb4-95fe-d8b3e80c6d7e"
  },
  "content": [
    {
      "sectionType": "audio",
      "audioUrl": "https://soundcloud.com/example/example",
      "caption": "I'\''m a caption",
      "title": "I'\''m a title",
      "transcript": "<p>A transcript with <strong>Formatting</strong></p>"
    },
    {
      "sectionType": "document",
      "title": "HT",
      "documentId": "157aa518-a4fb-4ed2-b9a7-c3a132a66ead"
    },
    {
      "sectionType": "image",
      "description": "string",
      "imageId": "72cd12fd-21dc-4fb4-ab6d-52e177e04755",
      "caption": "string"
    },
    {
      "sectionType": "external_link",
      "link": {
        "text": "string",
        "url": "string"
      },
      "summary": "string"
    },
    {
      "sectionType": "text",
      "text": "string"
    },
    {
      "sectionType": "video",
      "videoUrl": "https://www.youtube.com/watch?v=12345,
      "caption": "string",
      "title": "string",
      "transcript": "string"
    }
  ],
  "audience": [
    {
      "name": "new audience"
    }
  ],
  "format": {
    "name": "new format"
  },
  "topics": [
    {
      "name": "New topic"
    }
  ],
  "type": [
    {
      "name": "szdasd"
    }
  ],
  "relatedResources": [
    {
      "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    }
  ]
}
'
```

#### /resource-image

```
curl --request POST \
  --url 'http://resourcehub.lndo.site/resource-image?_format=json' \
  --header 'Content-Disposition: file; filename="Example.jpg"' \
  --header 'Content-Type: application/octet-stream' \
  --header 'x-api-key: a593d7966687162200bec7929aac03a9' \
  --data '.../9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAgICAgJCAkKCgkNDgwODRMREBARExwUFhQWFBwrGx8bGx8bKyYuJSMlLiZENS8vNUROQj5CTl9VVV93cXecnNEBCAgICAkICQoKCQ0ODA4NExEQEBETHBQWFBYUHCsbHxsbHxsrJi4lIyUuJkQ1Ly81RE5CPkJOX1VVX3dxd5yc0f/CABEIAqgCqAMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAAAQIDBAUGBwj/2gAIAQEAAAAA+cSsiKUXDQ7ZILE65RTUISSlXZGEnECUGTZGy+KsjZKJOdk7hO2KcmTTktMrJU+DFGbSlZe512VxlNkVBJRiNNwqUWQjOuxWxjZaOzTCwJFlk7JthODtJKFjvWyNvztAOx3TRByFXYhqNcU41ycKYOmGKOautWQtktd+vVOy6Vspqws1IscSYrU5RuNRoPnLaV9jnCacFKKsahXKBCIlDPXjxZqoNtjASCXU6G2662btTvuZZXNkpWVTbd5ff87RO+QydRGyE3FSqK0hxhDPh5+OsGSbG5CHouphXVs7nRvsnOx2TnGwVgSG5KeuHV...'
```

#### /resource-document

```
curl --request POST \
  --url 'http://resourcehub.lndo.site?_format=json' \
  --header 'Content-Disposition: file; filename="Example.pdf"' \
  --header 'Content-Type: application/octet-stream' \
  --header 'x-api-key: a593d7966687162200bec7929aac03a9' \
  --data '...+CmVuZG9iagp4cmVmCjAgNDEKMDAwMDAwMDAwMCA2NTUzNSBmIAowMDAwMDAxNjg3IDAwMDAwIG4gCjAwMDA0MTEwMjMgMDAwMDAgbiAKMDAwMDAwMDAwOSAwMDAwMCBuIAowMDAwMDAwMTE3IDAwMDAwIG4gCjAwMDAwMDAyMTcgMDAwMDAgbiAKMDAwMDAwMDMyNSAwMDAwMCBuIAowMDAwMDAwNDI1IDAwMDAwIG4gCjAwMDAwMDA1MzMgMDAwMDAgbiAKMDAwMDAwMDYzMyAwMDAwMCBuIAowMDAwMDAwNzQyIDAwMDAwIG4gCjAwMDAwMDA4NDMgMDAwMDAgbiAKMDAwMDAwMDk1MyAwMDAwMCBuIAowMDAwMDAxMDU0IDAwMDAwIG4gCjAwMDAwMDExNjQgMDAwMDAgbiAKMDAwMDAwMTI2NSAwMDAwMCBuIAowMDAwMDAxMzc1IDAwMDAwIG4gCjAwMDAwMDE0NzYgMDAwMDAgbiAKMDAwMDAwMTU4NiAwMDAwMCBuIAowMDAwMDAxNzkwIDAwMDAwIG4gCjAwMDAwMDE4ODEgMDAwMDAgbiAKMDAwMDAwMTk3NyAwMDAwMCBuIAowMDAwMDAyMDc2IDAwMDAwIG4gCjAwMDAwMDIxNzkgMDAwMDAgbiAKMDAwMDAwMjI2OCAwMDAwMCBuIAowMDAwMDAyMzYyIDAwMDAwIG4gCjAwMDAwMDI0NTkgMDAwMDAgbiAKMDAwMDAwMjU2MCAwMDAwMCBuIAowMDAwMDAyNjUzIDAwMDAwIG4gCjAwMDAwMDI3NDUgMDAwMDAgbiAKMDAwMDAwMjgzOSAwMDAwMCBuIAowMDAwMDAyOTM3IDAwMDAwIG4gCjAwMDAwMjQxNzQgMDAwMDAgbiAKMDAwMDEwMzgwNCAwMDAwMCBuIAowMDAwMTg5MjA1IDAwMDAwIG4gCjAwMDAyNDI5OTggMDAwMDAgbiAKMDAwMDI4NzYzNSAwMDAwMCBuIAowMDAwMzQ2MDExIDAwMDAwIG4gCjAwMDAzOTM3MDAgMDAwMDAgbiAKMDAwMDQxMTM0MCAwMDAwMCBuIAowMDAwNDExNDU5IDAwMDAwIG4gCnRyYWlsZXIKPDwKL1NpemUgNDEKL1Jvb3QgNDAgMCBSCi9JbmZvIDM5IDAgUgo+PgpzdGFydHhyZWYKNDExNTYzCiUlRU9G...'
```
