<?php

namespace Drupal\resourcehub_rest;

use League\OpenAPIValidation\PSR7\Exception\ValidationFailed;
use League\OpenAPIValidation\PSR7\ValidatorBuilder;
use Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * OpenapiValidator service.
 */
class OpenApiValidator {

  /**
   * The PSR-7 converter.
   *
   * @var \Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface
   */
  protected $httpMessageFactory;

  /**
   * Constructs an OpenapiValidator object.
   *
   * @param \Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface $http_message_factory
   *   The PSR-7 converter.
   */
  public function __construct(HttpMessageFactoryInterface $http_message_factory) {
    $this->httpMessageFactory = $http_message_factory;
  }

  /**
   * Method description.
   */
  public function validate(Request $request, OpenApiExceptionHandlerInterface $exceptionHandler) {
    $psrRequest = $this->httpMessageFactory->createRequest($request);

    $validator = (new ValidatorBuilder())->fromYamlFile(\Drupal::service('extension.path.resolver')->getPath('module',
        'resourcehub_rest') . '/swagger.yml')->getServerRequestValidator();
    try {
      $validator->validate($psrRequest);
    } catch (ValidationFailed $exception) {
      $exceptionHandler->handle($exception);
    }
  }

}
