<?php

namespace Drupal\resourcehub_rest;

use League\OpenAPIValidation\PSR7\Exception\Validation\InvalidBody;
use League\OpenAPIValidation\PSR7\Exception\ValidationFailed;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * OpenApiRestExceptionHandler service.
 */
class OpenApiRestExceptionHandler implements OpenApiExceptionHandlerInterface {

  public function handle(ValidationFailed $exception) {
    while ($exception instanceof InvalidBody) {
      $exception = $exception->getPrevious();
    }
    {
      $chain = $exception->dataBreadCrumb()->buildChain();
      $message = 'field ' . implode('->',
          $chain) . ': ' . $exception->getMessage();
      throw new UnprocessableEntityHttpException($message);
    }
  }

}
