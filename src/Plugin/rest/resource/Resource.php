<?php

namespace Drupal\resourcehub_rest\Plugin\rest\resource;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\resourcehub_rest\OpenApiRestExceptionHandler;
use Drupal\resourcehub_rest\OpenApiValidator;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Provides a REST interface to ResourceHub entities and their references.
 *
 * @RestResource (
 *   id = "resroucehub_resource",
 *   label = @Translation("ResourceHub resource"),
 *   uri_paths = {
 *     "canonical" = "/resource/{id}",
 *     "create" = "/resource"
 *   }
 * )
 */
class Resource extends ResourceBase implements DependentPluginInterface {

  /**
   * The node storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|mixed|object
   */
  protected $nodeStorage;

  /**
   * The media storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|mixed|object
   */
  protected $mediaStorage;

  /**
   * The paragraph storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|mixed|object
   */
  protected $paragraphsStorage;

  /**
   * The taxonomy term storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|mixed|object
   */
  protected $termStorage;

  /**
   * The OpenAPI validator service.
   *
   * @var \Drupal\resourcehub_rest\OpenApiValidator
   */
  protected $openapiValidator;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\resourcehub_rest\OpenApiValidator $openapiValidator
   *   The openapi validator service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    EntityTypeManager $entity_type_manager,
    OpenApiValidator $openapiValidator
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition,
      $serializer_formats, $logger);
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
    $this->mediaStorage = $entity_type_manager->getStorage('media');
    $this->paragraphsStorage = $entity_type_manager->getStorage('paragraph');
    $this->openapiValidator = $openapiValidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity_type.manager'),
      $container->get('resourcehub_rest.openapi_validator')
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param mixed $data
   *   Data to construct the ResourceHub resource..
   *
   * @return Response
   *   The HTTP response object.
   */
  public function post(Request $request, $data) {
    $this->openapiValidator->validate($request, (new OpenApiRestExceptionHandler()));
    $this->validateParagraphsReferences('primaryResource', [$data['primaryResource']]);
    $this->validateParagraphsReferences('content', $data['content']);
    if (isset($data['imageId']) && !$this->loadEntityByProperties($this->mediaStorage, 'image', ['uuid' => $data['imageId']])) {
      throw new UnprocessableEntityHttpException('No image found with the UUID ' . $data['imageId']);
    }

    try {
      /** @var \Drupal\node\Entity\Node $resource */
      $resource = $this->nodeStorage->create([
        'type' => 'resource',
        'status' => $data['published'],
      ]);
      $resource->setTitle($data['title']);
      $resource->get('resourcehub_summary')->setValue($data['summary'] ?? '');
      $resource->get('resourcehub_audience_intro')->setValue($data['audienceIntro'] ?? '');

      if (isset($data['imageId'])) {
        if ($media = $this->loadEntityByProperties($this->mediaStorage, 'image', ['uuid' => $data['imageId']])) {
          $resource->get('resourcehub_image')->appendItem($media);
        }
      }

      if (isset($data['audience'])) {
        $this->upsertTerms($resource->get('resourcehub_audience'),'resourcehub_audience',
          $data['audience']);
      }
      if (isset($data['topics'])) {
        $this->upsertTerms($resource->get('resourcehub_topics'), 'resourcehub_topics', $data['topics']);
      }
      if (isset($data['type'])) {
        $this->upsertTerms($resource->get('resourcehub_resource_type'), 'resourcehub_resource_type', $data['type']);
      }
      if (isset($data['format'])) {
        $this->upsertTerms($resource->get('resourcehub_resource_format'), 'resourcehub_format', [$data['format']]);
      }
      if (isset($data['primaryResource'])) {
        $this->createParagraphs($resource->get('resourcehub_primary_resource'), [$data['primaryResource']]);
      }
      if (isset($data['content'])) {
        $this->createParagraphs($resource->get('resourcehub_content'), $data['content']);
      }

      $resource->save();
      return new ResourceResponse(['resourceId' => $resource->uuid()]);
    }
    catch (\Exception $e) {
      throw new HttpException(500, 'Internal Server Error', $e);
    }

  }

  /**
   * Validate all references provided exist.
   *
   * @param string $field
   *   The API field we are validating.
   * @param array $sections
   *   An array of associate arrays which provide data for the paragraph.
   */
  protected function validateParagraphsReferences(string $field, array $sections) {
    foreach ($sections as $index => $section) {
      switch ($section['sectionType']) {
        case 'image':
          if (!$this->loadEntityByProperties($this->mediaStorage, 'image', ['uuid' => $section['imageId']])) {
            $message = 'Field ' . $field . '->' . $index . '->imageId: Media with UUID'. $section['imageId'] . ' does not exist';
            throw new UnprocessableEntityHttpException($message);
          }
          break;
        case 'document':
          if (!$this->loadEntityByProperties($this->mediaStorage, 'document', ['uuid' => $section['documentId']])) {
            $message = 'Field ' . $field . '->' . $index . '->documentId: Media with UUID'. $section['documentId'] . ' does not exist';
            throw new UnprocessableEntityHttpException($message);
          }
          break;
      }
    }
  }

  /**
   * Convenience method to load entities.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity type storage.
   * @param string $bundle
   *   The entity bundle ID.
   * @param array $props
   *   The props to load by.
   *
   * @return false|\Drupal\Core\Entity\EntityInterface
   *   The entity if one can be loaded otherwise FALSE.
   */
  protected function loadEntityByProperties(EntityStorageInterface $storage, string $bundle, array $props = []) {
    $bundleKey = $storage->getEntityType()->getKey('bundle');
    $props[$bundleKey] = $bundle;
    $existing = $storage->loadByProperties($props);
    if (!empty($existing)) {
      return reset($existing);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Create new paragraphs from the request data.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The paragraphs field to append the new paragraphs to.
   * @param array $data
   *   An array of associative arrays providing the data to create paragraphs.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createParagraphs(FieldItemListInterface $field, array $data) {
    foreach ($data as $datum) {
      $paragraph = NULL;
      switch ($datum['sectionType']) {
        case 'audio':
          $audioMedia = $this->mediaStorage->create(['bundle' => 'remote_audio']);
          $audioMedia->get('field_media_oembed_audio')->setValue($datum['audioUrl']);
          $audioMedia->save();
          $paragraph = $this->paragraphsStorage->create(['type' => 'resourcehub_audio']);
          $paragraph->get('resourcehub_audio')->appendItem($audioMedia);
          $paragraph->get('resourcehub_caption')->setValue($datum['caption'] ?? '');
          $paragraph->get('resourcehub_title')->setValue($datum['title'] ?? '');
          $paragraph->get('resourcehub_transcript')->setValue($datum['transcript'] ?? '');
          break;
        case 'document':
          $paragraph = $this->paragraphsStorage->create(['type' => 'resourcehub_document']);
          if ($documentMedia = $this->loadEntityByProperties($this->mediaStorage, 'document', ['uuid' => $datum['documentId']])) {
            $paragraph->get('resourcehub_document')->appendItem($documentMedia);
            $paragraph->get('resourcehub_title')->setValue($datum['title'] ?? '');
          }
          break;
        case 'image':
          $paragraph = $this->paragraphsStorage->create(['type' => 'resourcehub_image']);
          if ($imageMedia = $this->loadEntityByProperties($this->mediaStorage, 'image', ['uuid' => $datum['imageId']])) {
            $paragraph->get('resourcehub_image')->appendItem($imageMedia);
            $paragraph->get('resourcehub_caption')->setValue($datum['caption'] ?? '');
            $paragraph->get('resourcehub_image_description')->setValue($datum['description'] ?? '');
          }
          break;
        case 'external_link':
          $paragraph = $this->paragraphsStorage->create(['type' => 'external_link']);
          $paragraph->get('resourcehub_external_link')->setValue(['uri' => $datum['link']['url'], 'title' => $datum['link']['text']]);;
          $paragraph->get('resourcehub_link_summary')->setValue($datum['summary'] ?? '');
          break;
        case 'text':
          $paragraph = $this->paragraphsStorage->create(['type' => 'resourcehub_text']);
          $paragraph->get('resourcehub_text')->setValue($datum['text']);;
          break;
        case 'video':
          $videoMedia = $this->mediaStorage->create(['bundle' => 'remote_video']);
          $videoMedia->get('field_media_oembed_video')->setValue($datum['videoUrl']);
          $videoMedia->save();
          $paragraph = $this->paragraphsStorage->create(['type' => 'resourcehub_video']);
          $paragraph->get('resourcehub_video')->appendItem($videoMedia);
          $paragraph->get('resourcehub_caption')->setValue($datum['caption'] ?? '');
          $paragraph->get('resourcehub_title')->setValue($datum['title'] ?? '');
          $paragraph->get('resourcehub_transcript')->setValue($datum['transcript'] ?? '');
          break;
      }
      if ($paragraph) {
        $paragraph->save();
        $field->appendItem($paragraph);
      }
    }
  }

  /**
   * Upsert taxonomy terms by name and append to the field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field to append the new\existing terms to.
   * @param string $vid
   *   The vocabulary ID to look for the existing terms.
   * @param array $termData
   *   An array of associative arrays with a `name` key.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function upsertTerms(FieldItemListInterface $field, string $vid, array $termData) {
    foreach ($termData as $term) {
      if ($existingTerm = $this->loadEntityByProperties($this->termStorage, $vid, ['name' => $term['name']])) {
        $field->appendItem($existingTerm);
      }
      else {
        $newTerm = $this->termStorage->create(['vid' => $vid, 'name' => $term['name']]);
        $newTerm->save();
        $field->appendItem($newTerm);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRoute($canonical_path, $method) {
    $route = parent::getBaseRoute($canonical_path, $method);

    // Change ID validation pattern.
    if ($method != 'POST') {
      $route->setRequirement('id', '\d+');
    }

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

}
