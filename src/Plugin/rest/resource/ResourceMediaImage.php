<?php

namespace Drupal\resourcehub_rest\Plugin\rest\resource;

use Drupal\file\Plugin\rest\resource\FileUploadResource;
use Drupal\media\Entity\Media;
use Drupal\resourcehub_rest\OpenApiRestExceptionHandler;
use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a REST interface to ResourceHub image media entities.
 *
 * @RestResource (
 *   id = "resourcehub_media_image",
 *   label = @Translation("ResourceHub media - image"),
 *   uri_paths = {
 *     "create" = "/resource-image"
 *   }
 * )
 */
class ResourceMediaImage extends ResourceMediaBase {

  /**
   * {@inheritdoc}
   */
  public function post(Request $request, $entity_type_id = '', $bundle = '', $field_name = '') {
    $this->openApiValidator->validate($request, (new OpenApiRestExceptionHandler()));
    $response = parent::post($request, 'media', 'image', 'field_media_image');
    /** @var \Drupal\file\Entity\File $file */
    $file = $response->getResponseData();
    $media = Media::create([
      'name' => $file->getFilename(),
      'bundle' => 'image',
      'field_media_image' => [
        'target_id' => $file->id(),
        'alt' => $request->query->get('alt'),
      ]
    ]);
    $media->save();
    return new ModifiedResourceResponse(['imageId' => $media->uuid()]);
  }

}
