<?php

namespace Drupal\resourcehub_rest\Plugin\rest\resource;

use Drupal\media\Entity\Media;
use Drupal\resourcehub_rest\OpenApiRestExceptionHandler;
use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\resourcehub_rest\Plugin\rest\resource\ResourceMediaBase;

/**
 * Provides a REST interface to ResourceHub image media entities.
 *
 * @RestResource (
 *   id = "resourcehub_media_document",
 *   label = @Translation("ResourceHub media - document"),
 *   uri_paths = {
 *     "create" = "/resource-document"
 *   }
 * )
 */
class ResourceMediaDocument extends ResourceMediaBase {

  /**
   * {@inheritdoc}
   */
  public function post(Request $request, $entity_type_id = '', $bundle = '', $field_name = '') {
    $this->openApiValidator->validate($request, (new OpenApiRestExceptionHandler()));
    $response = parent::post($request, 'media', 'document', 'field_media_file');
    /** @var \Drupal\file\Entity\File $file */
    $file = $response->getResponseData();
    $media = Media::create([
      'name' => $file->getFilename(),
      'bundle' => 'document',
      'field_media_file' => [
        'target_id' => $file->id(),
      ]
    ]);
    $media->save();
    return new ModifiedResourceResponse(['documentId' => $media->uuid()]);
  }

}
