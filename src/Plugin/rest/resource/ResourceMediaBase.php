<?php

namespace Drupal\resourcehub_rest\Plugin\rest\resource;

use Drupal\Core\Config\Config;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\Plugin\rest\resource\FileUploadResource;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\resourcehub_rest\OpenApiValidator;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ResourceMediaBase extends FileUploadResource {

  /**
   * The OpenAPI validator service.
   *
   * @var \Drupal\resourcehub_rest\OpenApiValidator
   */
  protected $openApiValidator;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger,
    FileSystemInterface $file_system,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    AccountInterface $current_user,
    $mime_type_guesser,
    Token $token,
    LockBackendInterface $lock,
    Config $system_file_config,
    EventDispatcherInterface $event_dispatcher = NULL,
    OpenApiValidator $open_api_validator = NULL
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition,
      $serializer_formats, $logger, $file_system, $entity_type_manager,
      $entity_field_manager, $current_user, $mime_type_guesser, $token, $lock,
      $system_file_config, $event_dispatcher);
      $this->openApiValidator = $open_api_validator;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('file_system'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('current_user'),
      $container->get('file.mime_type.guesser'),
      $container->get('token'),
      $container->get('lock'),
      $container->get('config.factory')->get('system.file'),
      $container->get('event_dispatcher'),
      $container->get('resourcehub_rest.openapi_validator')
    );
  }

}
