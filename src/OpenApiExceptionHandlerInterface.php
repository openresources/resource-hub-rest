<?php

namespace Drupal\resourcehub_rest;

use League\OpenAPIValidation\PSR7\Exception\ValidationFailed;
/**
 * OpenApiExceptionHandlerInterface interface.
 */
interface OpenApiExceptionHandlerInterface {

  /**
   * Handle the Openapi validation exception.
   */
  public function handle(ValidationFailed $exception);

}
